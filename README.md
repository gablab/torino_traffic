# Torino traffic


## Muoversi a Torino

### How to use
Go to the `muoversi_a_torino` directory.
- Python and pipenv: run `pipenv install`, then `pipenv run jupyter-lab` for the notebooks.
- Selenium: download from https://www.selenium.dev/downloads/ and run with `java -jar ~/Downloads/selenium-server-4.7.2.jar standalone` while using it from the python code.
- Firefox.


## UTD
A data visualization representing the amount of traffic flow in different days and hours, as recorded by a number of detectors scattered in Torino.

### Data processing
The database (UTD19 (utd19.ethz.ch), https://www.nature.com/articles/s41598-019-51539-5) is processed in `UTD.ipynb` and a new database `df_to_mean.csv` is created, grouping for each {detector, hour of the day, day of the week} and averaging the values. 

### Data visualization
An [interactive visualization](https://observablehq.com/@gabrielelabanca/torino-traffic) has been derived from this new database, using D3.js and Observable.

<iframe width="100%" height="593" frameborder="0"
  src="https://observablehq.com/embed/@gabrielelabanca/torino-traffic?cell=viewof+in_values&cell=map"></iframe>


