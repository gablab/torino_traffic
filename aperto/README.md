# Using Aperto Data for an Analysis of Public Transport Speed


# Findings

## Heatmap
![heatmap](images/heatmap.png)
The heatmap representing the average speed at a given time of a given weekday allows to visualise the areas most affected by slowdowns. In the right plot a zoom over my neighborhood is shown, where the slowdowns affecting piazza Baldissera (top roundabout), corso Novara (the east-bound road departing from it) and corso Principe Oddone (the south-bound road) are visible: these traffic jams have made their way in the news, so it was a primary goal of my analysis being able to detect them. Another slow section is visible in via Cigna, roughly at the center of the map. The southern slow areas are probably connected to the generally slower traffic in the central area of Torino, and to the presence of a hub where the vehicles of many lines stop between routes.

## Speed by hour and day
![speed_lineplot](images/speed_by_hour_day.png)
This plot is simple and allows first of all to validate the results with common sense: it shows the average speed for every line in a given time of the day, on a given day of the week.

The choice of colours and line styles highlights the big difference between the business week and the weekend: while in the business days the speed is on average lower, with lower peaks in the morning and in the afternoon (commuting hours), the weekends show lower speeds in the early afternoon. Another difference is the fact that, while usually faster in the weekends, the speed is lower on Saturday night, reflecting the increase of traffic due to the night life. Interestingly, around 20h, from Monday to Friday the traffic seems increasing: this is a small effect, and not one of high confidence, but it makes sense with the notion that more people go out for dinner towards the weekend.



# Technical details
The data is collected from a real-time service, stored in a self-hosted database and then collected and aggregated in a Jupyter notebook with the usual Python libraries.

The speed is calculated as the variation of position over the 


## Real-time data
Visit http://aperto.comune.torino.it/dataset/feed-gtfs-real-time-trasporti-gtt/resource/65084c1e-cfca-4232-9509-d949f6a70d33: they provide an asfx link that gets a protocol buffer file: http://percorsieorari.gtt.to.it/das_gtfsrt/vehicle_position.aspx.

See https://www.freecodecamp.org/news/googles-protocol-buffers-in-python/#what-are-protocol-buffers-and-how-do-they-work for information about using protocol buffers. The `.proto` file can be found at https://translink.com.au/sites/default/files/acquiadam-documents/gtfs-realtime.proto.

One week of data amounts to ~220MB.

Run `vehicle_positions.py` to start the continuous acquisition of data. `docker-compose.yml` contains the specification of a PostgreSQL database capable of ingesting the data, and which will be queried by the notebook.

## List of lines
This python3 script can be used to output a list of all the transport lines
```
from bs4 import BeautifulSoup
from urllib.parse import urlparse, parse_qs
import requests

html_doc = requests.get('https://www.gtt.to.it/cms/percorari/urbano?bacino=U&view=linee').text
soup = BeautifulSoup(html_doc, 'html.parser')

print([parse_qs(urlparse(x.find('a').get('href')).query)['linea'][0] for x in soup.find_all('table')[1].find_all('tr')[1:]])
```

