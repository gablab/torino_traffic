CREATE TYPE OccupancyStatus AS ENUM ('CRUSHED_STANDING_ROOM_ONLY', 'EMPTY', 'FEW_SEATS_AVAILABLE', 'FULL', 'MANY_SEATS_AVAILABLE', 'NOT_ACCEPTING_PASSENGERS', 'STANDING_ROOM_ONLY');

CREATE TYPE RouteId AS ENUM ('METRO', 'M1S', '3', '4', '4N', '7', '9', '10', '10N', '13', '13N', '15', '16CD', '16CS', '79', 'SF2', 'ST1', '2', '5', '5B', '6', '8', '11', '12', '14', '17', '17B', '19', '19N', '20', '21', '22', '24', '25', '27', '29', '30', '32', '33', '34', '35', '35N', '36', '36N', '38', '38S', '39', '40', '41', '42', '43', '44', '44S', '45', '45B', '46', '46N', '47', '48', '49', '50', '51', '52', '53', '54', '55', '56', '57', '58', '58B', '59', '59B', '60', '61', '62', '63', '63B', '64', '65', '66', '67', '68', '69', '70', '71', '72', '72B', '73', '74', '75', '76', '77', '78', '79B', '80', '81', '82', '83', '84', '86', '86B', '88', '89', '89B', '90', '91', '92', '93B', '94', '95', '95B', '96', '97', '98', '99', '102', 'CP1', 'OB1', 'RV2', 'SE1', 'SE2', 'SM1', 'SM2', 'VE1', 'VE2', '1C', '2C', '1N', 'W01', 'N04', 'S04', 'S05', 'S08', 'N08', 'N10', 'W15', 'W60', 'E68', '3990', '3902');

create table vehicle_positions (
  id serial not null primary key,
  vehicle_id integer not null,
  route_id RouteId not null,
  ts timestamp without time zone not null,
  lat float not null,
  lon float not null,
  bearing integer not null,
  occupancy OccupancyStatus not null
);

create unique index vehicle_positions_unique_index on vehicle_positions (vehicle_id, ts);
