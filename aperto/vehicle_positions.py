import time
from gtfs_realtime_pb2 import FeedMessage as GTFS  # protobuf
import datetime
import requests
import json
import psycopg2
from psycopg2.extras import execute_batch


def get_gtfs():
    r = requests.get('http://percorsieorari.gtt.to.it/das_gtfsrt/vehicle_position.aspx')

    gtfs = GTFS()
    gtfs.ParseFromString(r.content)
    return gtfs


def get_positions(gtfs):    
    all_route_ids = {v.vehicle.trip.route_id for v in gtfs.entity}

    occupancy_status_dict = {
        0: 'EMPTY',
        1: 'MANY_SEATS_AVAILABLE',
        2: 'FEW_SEATS_AVAILABLE',
        3: 'STANDING_ROOM_ONLY',
        4: 'CRUSHED_STANDING_ROOM_ONLY',
        5: 'FULL',
        6: 'NOT_ACCEPTING_PASSENGERS'
    }
    all_occupancy_statuses = {occupancy_status_dict[v.vehicle.occupancy_status] for v in gtfs.entity}
    
    return [
        {
            "vehicle_id": v.id,
            "route_id": v.vehicle.trip.route_id[:-1],
            "ts": datetime.datetime.fromtimestamp(v.vehicle.timestamp),
            "lat": v.vehicle.position.latitude,
            "lon": v.vehicle.position.longitude,
            "bearing": v.vehicle.position.bearing,
            "occupancy": occupancy_status_dict[v.vehicle.occupancy_status]
        }
        for v in gtfs.entity
    ]


def insert_into_vehicle_positions(vehicle_positions):
    conn = psycopg2.connect(database="traffic",
                            host="localhost",
                            user="postgres",
                            password="password",
                            port="55433")

    cursor = conn.cursor()

    query = '''
        INSERT INTO vehicle_positions 
        (vehicle_id, route_id, ts, lat, lon, bearing, occupancy) 
        VALUES (%(vehicle_id)s, %(route_id)s, %(ts)s, %(lat)s,
            %(lon)s, %(bearing)s, %(occupancy)s
        )
        ON CONFLICT DO NOTHING
    '''

    execute_batch(cursor, query, vehicle_positions)
    conn.commit()


log_file = '.logs_vehicle_positions'
    
if __name__ == '__main__':
    while True:
        with open(log_file, 'w') as f:
            f.write(datetime.datetime.now())
        gtfs = get_gtfs()
        try:
            vehicle_positions = get_positions(gtfs)
            insert_into_vehicle_positions(vehicle_positions)

            with open(log_file, 'w') as f:
                f.write(f'    inserted: {len(vehicle_positions)} rows')
            time.sleep(120)
        except BaseException as e:
            with open(log_file, 'w') as f:
                f.write(e)
            time.sleep(10)
