"""
geo_utils
"""

import numpy as np
import pandas as pd

import dask
from dask.distributed import Client
# limit memory usage: https://stackoverflow.com/a/59887066/5599687
# remove warnings: https://stackoverflow.com/a/57461001/5599687
import dask.dataframe as dd # https://tutorial.dask.org/04_dataframe.html, in particular map_partitions


from bokeh.plotting import figure
from bokeh.tile_providers import OSM, CARTODBPOSITRON, get_provider
from bokeh.models import HoverTool


k_geo_convert = 6378137

def webmercator_to_wgs84_lon(x):
    return 180.0*x/(k_geo_convert*np.pi)

def webmercator_to_wgs84_lat(y):
    return 360.0/np.pi * np.arctan(np.exp(y/k_geo_convert)) - 90

def webmercator_to_wgs84(df, x="x", y="y"):
    df_return = df.assign(
            lon=lambda d: webmercator_to_wgs84_lon(d[x]),
            lat=lambda d: webmercator_to_wgs84_lat(d[y])
            )
    return df_return

def wgs84_lon_to_webmercator(lon):
    return lon * (k_geo_convert * np.pi/180.0)

def wgs84_lat_to_webmercator(lat):
    return np.log(np.tan((90 + lat) * np.pi/360.0)) * k_geo_convert

def wgs84_to_webmercator(df, lon="lon", lat="lat", web_x='x', web_y='y'):
    """Converts decimal longitude/latitude to Web Mercator format"""
    df_return = df.assign(
        wmx=lambda d: wgs84_lon_to_webmercator(d[lon]),
        wmy=lambda d: wgs84_lat_to_webmercator(d[lat]))
    df_return = df_return.rename(
        columns={
            "wmx": web_x, "wmy": web_y
    })
    return df_return





def create_delta_columns(df):
    """
    For each entry evaluate distance and duration from previous one.
    """
    # init columns
    df_return = df.assign(
        lat0=lambda x: x.shift().lat,
        lat1=lambda x: x.lat,
        lon0=lambda x: x.shift().lon,
        lon1=lambda x: x.lon,
        t0=lambda x: x.shift().time_stamp,
        t1=lambda x: (x.time_stamp),
        soc0=lambda x: x.shift().state_of_charge, soc1=lambda x: x.state_of_charge)
    if isinstance(df, pd.DataFrame):
        df_return.t0 = pd.to_datetime(df_return.t0)
        df_return.t1 = pd.to_datetime(df_return.t1)
    if isinstance(df, dask.dataframe.DataFrame):
        df_return['t0'].map_partitions(pd.to_datetime).compute()
        df_return['t1'].map_partitions(pd.to_datetime).compute()
    df_return = df_return.assign(
        eucl_distance=lambda x: np.sqrt(np.power(x.lat0-x.lat1, 2) + np.power(x.lon0-x.lon1, 2)),
        delta_time=lambda x: x.t1-x.t0,
        delta_soc=lambda x: x.soc1-x.soc0
    )
    if isinstance(df, pd.DataFrame):
        df_return.delta_time = pd.to_timedelta(df_return.delta_time)
    if isinstance(df, dask.dataframe.DataFrame):
        df_return['delta_time'].map_partitions(pd.to_timedelta).compute()
    return df_return






def get_tiled_figure(lon_limits, lat_limits, title=None, hover=None):
    tile_provider = get_provider(OSM)
    # supply range bounds in web mercator coordinates
    tools =  ["pan","wheel_zoom,box_zoom,reset"]
    if hover is not None:
        tools.append(hover)
    p = figure(title=title,
        x_range=(wgs84_lon_to_webmercator(lon_limits[0]), 
            wgs84_lon_to_webmercator(lon_limits[1])),
        y_range=(wgs84_lat_to_webmercator(lat_limits[0]), 
            wgs84_lat_to_webmercator(lat_limits[1])),
        x_axis_type="mercator", y_axis_type="mercator",
        match_aspect=True, tools=tools)
    p.add_tile(tile_provider)
    return p


def describe_dataset(input_name, input_encoding='utf-8'):
    """
    Describes the csv file `input_name`.
    """
    with Client(memory_limit='3GB', processes=False,
                n_workers=1, threads_per_worker=2, silence_logs='error') as client:
        ddf_input = dd.read_csv(input_name, encoding=input_encoding, dtype={'error': 'float64', 'flow': 'float64'})
        for col in ['city', 'day']:
            print(col, [un for un in ddf_input[col].unique()])


def query_dataset(input_start_date, input_end_date, input_city, 
        output_name, input_name, input_encoding='utf-8'):
    """
    Creates a new file called `output_name`, selecting from the 
    file `input_name` only the entries in the date range and 
    for the required city.
    """
    with Client(memory_limit='3GB', processes=False,
                n_workers=1, threads_per_worker=2, silence_logs='error') as client:
        ddf_input = dd.read_csv(input_name, encoding=input_encoding, dtype={'error': 'float64', 'flow': 'float64'})
        start_date = pd.Timestamp(input_start_date)
        end_date = pd.Timestamp(input_end_date)
        ddf_input.day = dd.to_datetime(ddf_input.day)
        ddf_ranged = ddf_input.loc[lambda x: x.day > start_date].loc[lambda x: x.day < end_date].loc[lambda x: x.city == input_city]#.compute()
        
        #ddf_ranged = ddf_ranged.drop(axis=1, labels=['id', 'provider', 'title'])
        
        #ddf_ranged.columns = ['day', 'interval', 'detid', 'flow', 'occ', 'error', 'city', 'speed']
        #ddf_ranged = ddf_ranged.map_partitions(geo_utils.create_delta_columns) NO! it works only when a single vehicle is present
        ddf_ranged = ddf_ranged.compute()
        ddf_ranged.to_csv(output_name)
        





